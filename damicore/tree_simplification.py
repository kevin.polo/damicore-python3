#!/usr/bin/python

from math import log10, ceil
from random import Random

import numpy as np

from tree import Node, Leaf, Edge


def num_digits(x):
    """Returns number of decimal digits in x.

    >>> num_digits(99)
    2
    >>> num_digits(100)
    2
    >>> num_digits(101)
    3
    """
    return int(ceil(log10(x)))


def artificial_ids(n):
    """Generates n artificial ids."""
    return ['_n{num:>0{max}}'.format(num=i, max=num_digits(n))
            for i in range(n)]


def matrix_argmin(m):
    """Returns indices of the minimum value in m."""
    indices = range(len(m))

    return min([(i, j) for i in indices for j in indices],
               key=lambda pair: m[pair[0]][pair[1]])
               # key = lambda (i,j): m[i][j]) #In last version


def calculate_q(m, sums):  # sums was a map, but now it is the list retruned by the map
    """Calculates matrix Q of the neighbor joining algorithm.

            The value Q_{ij} gives the decrease of total sum of edge lengths in the
    tree if nodes i and j are joined.
    """
    n = len(m)
    indices = range(n)
    q = [[0.0 for _ in indices] for _ in indices]

    #sums =  list(sum_map) #'map' object is not subscriptable, but it was in Python 2

    for i, row in enumerate(m):
        for j, dij in enumerate(row):
            q[i][j] = (n - 2) * dij - sums[i] - sums[j]

    for i in indices:
        q[i][i] = float("inf")

    return q


def update_distance_matrix(m, sums, i, j, is_disallow_negative_lengths=False):  # sums was a map, but now it is the list return by the map
    """Returns a new distance matrix with nodes i and j joined.

            The tuple (i,j) must be sorted. A new distance matrix is created with
    nodes i and j joined under a new node X. The node is placed at index i, and
    all its distances are updated.

    @return (new_m, di, dj) new distance matrix; distances i->X and j->X
    """

    n = len(m)
    # sums =  list(sums) #'map' object is not subscriptable, but it was in Python 2

    dij, si, sj = m[i][j], sums[i], sums[j]

    di = (dij + (si - sj)/(n - 2))/2
    dj = (dij + (sj - si)/(n - 2))/2
    if is_disallow_negative_lengths:
        if di <= 0: di, dj = 0, dij
        elif dj <= 0: di, dj = dij, 0
    if dij < 0:
        print(dij)
    dk = [(m[i][k] + m[j][k] - dij)/2 for k in range(n)]

    new_m = [[dij for dij in row] for row in m]

    for k in range(n):
        new_m[i][k] = new_m[k][i] = dk[k]

    new_m.pop(j)
    for row in new_m:
        row.pop(j)

    return new_m, di, dj


def join_neighbors(tree, i, j, di, dj):
    r"""Returns a new tree with nodes i and j joined under a new node.

            [ (n_0)  ...  (n_i) ... (n_j)  ...  (n_N) ] --->

            [ (n_0)  ...     ()      ...  (n_{N-1}) ]
                                      di /  \
                                          /    \ dj
                                      (n_i)   \
                                                  (n_j)
    """
    new_tree = [node for node in tree]
    new_tree[i] = Node(None,
                       Edge(tree[i], di),
                       Edge(tree[j], dj))
    new_tree.pop(j)
    return new_tree


def neighbor_joining(m, ids=None, is_disallow_negative_lengths=False):
    """Neighbor Joining algorithm.

            Given a distance matrix, the algorithm seeks a tree that approximates the
    measured distances by greedily choosing to join the pair of elements that
    minimize the total sum of edge lengths.
    """
    n = len(m)
    if ids is None:
        ids = artificial_ids(n)

    # Turn m symmetric and floating-point if it's not already
    m = np.array(m, dtype='float')
    m = m + np.transpose(m)

    tree = [Leaf(id_) for id_ in ids]

    for _ in range(n, 2, -1):
        # Find closest neighbors
        s = map(sum, m)
        s_list = list(s)
        q = calculate_q(m, s_list)

        # Join neighbors and update distance matrix
        i, j = sorted(matrix_argmin(q))

        m, di, dj = update_distance_matrix(m, s_list, i, j, is_disallow_negative_lengths)
        tree = join_neighbors(tree, i, j, di, dj)

    d = m[0][1]
    return join_neighbors(tree, 0, 1, d/2, d/2)[0]


def get_tree(node, parent_dist, leaf_names):
    r"""Convert sciply.cluster.hierarchy.to_tree()-output to a Tree format.

    :param node: output of sciply.cluster.hierarchy.to_tree()
    :param parent_dist: output of sciply.cluster.hierarchy.to_tree().dist
    :param leaf_names: list of leaf names
    :returns: tree in Newick format
    """
    if node.is_leaf():
        return Leaf(leaf_names[node.id])
    else:
        left_node = get_tree(node.get_left(), node.dist, leaf_names)
        right_node = get_tree(node.get_right(), node.dist, leaf_names)
        return Node(None,
                    Edge(right_node, node.dist-node.get_right().dist),
                    Edge(left_node, node.dist-node.get_left().dist))


def agglomerative_clustering(m, ids=None, simplification_step_name='average-linkage'):
    """Linkage algorithm.

        Given a distance matrix, the algorithm seeks a tree that approximates the
    measured distances by greedily choosing to join the pair of elements that
    minimize the total sum of edge lengths.
    """
    from scipy.cluster.hierarchy import linkage, to_tree
    import scipy.spatial.distance as ssd
    m = ssd.squareform(m)
    linkage_matrix = linkage(m, simplification_step_name.split("-")[0])
    tree = to_tree(linkage_matrix, False)
    #from pdb import set_trace
    #set_trace()
    return get_tree(tree, tree.dist, ids)


def get_tree_toytree(node):
    if node.is_leaf():
        return Leaf(node.name.strip("'"))
    else:
        return Node(None,
                    *[Edge(get_tree_toytree(child),child.dist) for child in node.children])
        #edges = []
        #for child in node.children:
        #    edges = get_tree_toytree(child)
        #left_node = get_tree_toytree(node.children[1])
        #right_node = get_tree_toytree(node.children[0])
        #return Node(None,
        #            Edge(right_node, np.float64(node.children[0].dist)),
        #            Edge(left_node, np.float64(node.children[1].dist)))


def get_toytree_ete3(tree, dist=0.0):
    from toytree import TreeNode
    if tree.is_leaf():
        node = TreeNode.TreeNode(dist=dist, name=tree.name)
        return node
    else:
        node = TreeNode.TreeNode(dist=dist)
        for child in tree.children:
            node.add_child(get_toytree_ete3(child,child.dist))
        return node

def load_tree(tree_source):
    import toytree
    if "," not in tree_source:
        tree = toytree.tree(tree_source)
    else:
        trees = []
        for filename in tree_source.split(","):
            trees.append(toytree.tree(filename))
        trees = toytree.mtree(trees)
        tree = trees.get_consensus_tree()
    names = [n.strip("'") for n in tree.get_tip_labels()]
    tree = tree.treenode
    tree = get_tree_toytree(tree)
    return tree, names

def _random_joining(ids):
    """Generates a tree by repeatedly joining elements randomly."""
    r = Random()
    tree = [Leaf(id_) for id_ in ids]
    for _ in range(len(ids) - 1):
        i, j = sorted(r.sample(range(len(tree)), 2))
        tree = join_neighbors(tree, i, j, r.random(), r.random())

    return tree[0]


if __name__ == '__main__':
    from tree import test_tree
    expected_tree, m, ids = test_tree()
    tree = neighbor_joining(m, ids)

    print(tree, expected_tree)

    # Random test
    from tree import distance_matrix
    expected_tree = _random_joining(map(str, range(10)))
    m, (ids, _) = distance_matrix(expected_tree)
    tree = neighbor_joining(m, ids)
    print(tree, expected_tree)
